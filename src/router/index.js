import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/layout/Main/Index'
import Invoice from '@/pages/Invoice/Index'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index,
      props: true
    },
    {
      path: '/invoice',
      name: 'Invoice',
      component: Invoice,
      props: true
    }
  ]
})
