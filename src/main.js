// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueLocalStorage from 'vue-localstorage'
// import VueProgressBar from 'vue-progressbar'
import axios from 'axios'
import moment from "moment"
import VueMomentJS from "vue-momentjs"
import VueAxios from 'vue-axios'
// import vueMoment from 'vue-moment'
import App from './App'
import router from './router'

// Vue.use(VueProgressBar, {
//   color: '#D7803B',
//   height: '2px',
//   failedColor: 'red'
// })
Vue.use(VueAxios, axios)
axios.defaults.baseURL = process.env.API_LOCATION
Vue.config.productionTip = false
Vue.use(VueLocalStorage, {
  name: 'ls',
  bind: true
})
Vue.use(VueMomentJS, moment)
// Vue.use(vueMoment);

Vue.mixin({
  filters: {
    currency (str) {
      var r
      if (str != undefined || str != '') {
        if (str.includes('.00')) {
          r = str.replace('.00', '')
        }
        return 'Rp. '+r
      }
    }
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
